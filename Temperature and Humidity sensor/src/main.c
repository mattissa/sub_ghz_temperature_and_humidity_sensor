/*
 * Created by
 * Mattis Spieler Asp
 */
 /**
 * Temperature and Humidity sensor
 */
#include <asf.h>
#define MISO PB6
#define MOSI PB5
#define SCK PB7
#define CHIPSEL PB4
#define RESET PC1
#define MISC PC0
#define INTERUPT PD0
#define SLPTR PD1


#define F_CPU 8000000
#define BAUD 9600
#define BAUDRATE ((F_CPU)/(BAUD*16UL)-1)
//#include <avr/io.h>
#include <util/delay.h>

void uart_init(){
	//UCSR1C |= (1 << UMSEL01) //finne riktig umsel, at den er async.
	//baudrate generator async norm, u2x1 = 0
	UBRR1H = (BAUDRATE >> 8);
	UBRR1L = BAUDRATE;
	
	//UCSR1B
	UCSR1C |= (1 << UMSEL01) |(1 << UCSZ11) | (1 << UCSZ10);
	
	//enable transmitter and receiver
	UCSR1B |= (1 << TXEN1) | (RXEN1);
	DDRD = DDRD | ( 1 << PIND3);
	
	
}

void USART_Transmit(char data){
	
	
	while(!(UCSR1A & (1 << UDRE1)));
	UDR1 = data;	
	
}

void SPI_init(){
	
	DDRB |= (1 << CHIPSEL) |(1 << MOSI) | (1 << SCK); //set CHIPSEL, SCK and MOSI to output.
	DDRB &= ~(1 <<MISO) ; // MISO INPUT
	//PORTB |= (1 << CHIPSEL); // SS HIGH
	// SPDR initialiserer data transmission
	SPCR |= (1 << SPR0) | (1 << SPR1) | (1 << MSTR) | (1 << SPE); //set prescale 128, set master, SPI ENABLE
	PORTB |= (1 << CHIPSEL);
	

}

void SPI_Transmit(char data){
	PORTB &= ~(1 << CHIPSEL); // Low = transmit mode
	//start transmission
	SPDR = data;
	//
	
	
	//wait for transmission complete
	while(!(SPSR & (1 << SPIF)));
	PORTB |= (1 << CHIPSEL);
}

int main (void){
	// Insert system clock initialization code here (sysclk_init()).

	board_init();
	SPI_init();
	while(1){
		SPI_Transmit('U');
		_delay_ms(1000);
	}
	// Insert application code here, after the board has been initialized.
	
	
	//DDRB = 0b00000001; //set port B pins 1-7 as inputs, pin 0 as input
	//DDRB |= 1 << PINB0; // set DDRB for pin0 to 1 - output - dont change the rest
	
	
	/*	
	while(1){
		PORTB ^= 1 << PORTB0; //change state of portB pin 0
		_delay_ms(800);
	}
	*/
}


//DDRB velger om en pin er input eller output
//portB setter verdi p� pin
//pinb leser input og output p� portB


